# *UNSUPPORTED* bindist of GHC with interactive frontend support

This repository hosts binary distribution tar balls compiled from
this experimental branch of GHC:

	https://gitlab.haskell.org/complyue/ghc/tree/ghc-8.6-ife

## Lack of Support

GHC installation by tar balls from this repository is meant for
quick try-outs to see if it happens work for one's environment,
and is supported by NO party.

One is encouraged to compile from soure to use GHC with the 
experimental feature included, so can use standard support of
GHC.

If you are sure to have encountered problems specific to the
interactive frontent feature, file an issue at 
	https://gitlab.haskell.org/complyue/ghc/issues
and let's see what can be done.

## Building Environment

The binaries here for macOS is built on Mojave, for Linux is
built on Ubuntu 18.04.

## Maintanence

This repository does NOT accept MRs, if you'd like to host more
bindist tar balls for the interactive frontend branch, please do
that from your own repository.


## Usage

Append the following content into your project's `stack.yaml`:

```yaml
compiler-check: match-exact

# CAVEATS
#
#   binary distributions referenced following have NO support, checkout:
#
# https://gitlab.haskell.org/complyue/ghc-ife-bindist/blob/master/README.md

ghc-variant: ife

setup-info:
  ghc:
    macosx-custom-ife:
      8.6.5:
        url: "https://gitlab.haskell.org/complyue/ghc-ife-bindist/raw/master/ghc-8.6.5-x86_64-apple-darwin.tar.gz2"
    linux64-custom-ife:
      8.6.5:
        url: "https://gitlab.haskell.org/complyue/ghc-ife-bindist/raw/master/ghc-8.6.5-x86_64-unknown-linux.tar.xz"

```

Then setup & build your project normally.
