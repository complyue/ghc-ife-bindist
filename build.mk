

# include mk/flavours/quick.mk
include mk/flavours/perf.mk
# include mk/flavours/dwarf.mk

BUILD_SPHINX_HTML  = NO
BUILD_SPHINX_PDF   = NO
BUILD_MAN          = NO


# Don't strip debug and other unneeded symbols from libraries and executables.
STRIP_CMD = :
